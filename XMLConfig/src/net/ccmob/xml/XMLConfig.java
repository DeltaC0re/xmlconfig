package net.ccmob.xml;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XMLConfig {

	/**
	 * This File is licensed under the MIT License (file -> license)
	 *
	 * @author Marcel Benning
	 * @email marcel@ccmob.net
	 * @website https://ccmob.net
	 * @version 1.0
	 *
	 */

	/**
	 * Here is the config saved to and can be modified at runtime. After
	 * modification, call the save method.
	 */
	private XMLNode	rootNode	= new XMLNode("XMLConfig");

	/**
	 * Stores the fileName if given;
	 */
	private String	fileName	= null;

	/**
	 * @param filename
	 *            the file to parse from
	 */
	public XMLConfig(String filename) {
		File f = new File(filename);
		this.setFileName(f.getAbsolutePath());
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			this.parse();
		}
	}

	/**
	 * @param f
	 *            the file to parse from
	 */
	public XMLConfig(File f) {
		this.setFileName(f.getAbsolutePath());
		this.parse();
	}

	/**
	 * @param lines
	 *            - the file content to parse from
	 */
	public XMLConfig(ArrayList<String> lines) {
		this.parseLines(lines);
	}

	/**
	 * @param lines
	 *            the file content to parse from
	 */
	public XMLConfig(String[] lines) {
		ArrayList<String> arr = new ArrayList<String>();
		for (int i = 0; i < lines.length; i++) {
			arr.add(lines[i]);
		}
		this.parseLines(arr);
		arr.clear();
	}

	/**
	 * This function converts the content of the file - specified at the
	 * constructor - to a XMLNode tree, saved in the rootNode varibale.
	 */
	public void parse() {
		try {
			this.setRootNode(XMLParser.parseFile(this.getFileName(), this));
		} catch (EOFException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This function converts the content of the ArrayList (the content of a
	 * file or a string in the first object) to a XMLNode tree, saved in the
	 * rootNode varibale.
	 */
	public void parseLines(ArrayList<String> lines) {
		try {
			this.setRootNode(XMLParser.parseFileLines(lines, this));
		} catch (EOFException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This function saves the rootNode to a xmlFile ( @param filename ) with
	 * proper format
	 *
	 * @param filename
	 */
	public void save(String filename) {
		try {
			File f = new File(filename);
			/*
			 * if (f.exists()) {
			 * f.delete();
			 * }
			 */
			FileWriter writer = new FileWriter(f, false);
			this.writeNode(this.getRootNode(), 0, writer);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void save() {
		if (this.getFileName() != null) {
			this.save(this.getFileName());
		} else {
			System.out.println("Got no file name to save config.");
		}
	}

	/**
	 * formats a XMLAttribute
	 *
	 * @param attr
	 * @return formated attribute string
	 */
	private String formAttribute(XMLAttribute attr) {
		return attr.getName() + "=\"" + attr.getValue() + "\"";
	}

	/**
	 * Creates a string with tabs for saving
	 *
	 * @param tabIndex
	 * @return
	 */
	private String formTabs(int tabIndex) {
		String tabs = "";
		for (int i = 0; i < tabIndex; i++) {
			tabs += "\t";
		}
		return tabs;
	}

	/**
	 * This function writes an XMLNode ( @param node ) to a FileWriter ( @param
	 * writer ) with the proper format. The @param tabIndex specifies the shift
	 * to the right in the file for proper reading.
	 *
	 * @param node
	 * @param tabIndex
	 * @param writer
	 * @throws IOException
	 */
	private void writeNode(XMLNode node, int tabIndex, FileWriter writer) throws IOException {
		String line = this.formTabs(tabIndex) + "<" + node.getName();
		if (node.getAttributes().size() > 0) {
			for (int i = 0; i < node.getAttributes().size(); i++) {
				line += " " + this.formAttribute(node.getAttributes().get(i));
			}
		}
		if (node.getValue() != null) {
			line += ">" + node.getValueS() + "</" + node.getName() + ">\n";
			writer.write(line);
		} else {
			if (node.getChilds().size() == 0) {
				line += "></" + node.getName() + ">";
				writer.write(line + "\n");
			} else {
				line += ">";
				writer.write(line + "\n");
				for (XMLNode child : node.getChilds()) {
					this.writeNode(child, tabIndex + 1, writer);
				}
				writer.write(this.formTabs(tabIndex) + "</" + node.getName() + ">\n");

			}
		}
	}

	/**
	 * @return the rootNode
	 */
	public XMLNode getRootNode() {
		return this.rootNode;
	}

	/**
	 * @param rootNode
	 *            the rootNode to set
	 */
	public void setRootNode(XMLNode rootNode) {
		this.rootNode = rootNode;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return this.fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public static class XMLObject {

		protected String	name	= null;
		protected Object	value	= null;

		/**
		 * @return the name
		 */
		public String getName() {
			return this.name;
		}

		/**
		 * @param name
		 *            the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @return the value
		 */
		public Object getValue() {
			return this.value;
		}

		/**
		 * @return the value
		 */
		public String getValueS() {
			return String.valueOf(this.getValue());
		}

		/**
		 * @return the value
		 */
		public double getValueD() {
			return Double.valueOf(this.getValueS());
		}

		/**
		 * @return the value
		 */
		public float getValueF() {
			return Float.valueOf(this.getValueS());
		}

		/**
		 * @return the value
		 */
		public int getValueI() {
			return Integer.valueOf(this.getValueS());
		}

		/**
		 * @param value
		 *            the value to set
		 */
		public void setValue(Object value) {
			this.value = value;
		}

		/**
		 * @param value
		 *            the value to set
		 */
		public void setValueS(String value) {
			this.value = value;
		}

		/**
		 * @param value
		 *            the value to set
		 */
		public void setValueD(Double value) {
			this.value = value;
		}

		/**
		 * @param value
		 *            the value to set
		 */
		public void setValueF(Float value) {
			this.value = value;
		}

		/**
		 * @param value
		 *            the value to set
		 */
		public void setValueI(Integer value) {
			this.value = value;
		}

	}

	public static class XMLAttribute extends XMLObject {

		/**
		 * @param name
		 *            Attribute name
		 * @param value
		 *            Attribute value
		 */
		public XMLAttribute(String name, Object value) {
			this.setName(name);
			this.setValue(value);
		}

		/**
		 * Just without the value
		 *
		 * @param name
		 *            Attribute name
		 */
		public XMLAttribute(String name) {
			this(name, null);
		}

	}

	public static class XMLNode extends XMLObject {

		/**
		 * Node childs
		 */
		private ArrayList<XMLNode>		childs;

		/**
		 * Node attributes
		 */
		private ArrayList<XMLAttribute>	attributes;

		/**
		 * The parent of this node
		 */
		private XMLNode					parent;

		public XMLNode() {
			this.childs = new ArrayList<XMLNode>();
			this.attributes = new ArrayList<XMLAttribute>();
		}

		/**
		 * The name of the node:
		 *
		 * @param name
		 */
		public XMLNode(String name) {
			this();
			this.setName(name);
		}

		/**
		 * Internal add function
		 */
		private void addAttributePrivate(XMLAttribute attr) {
			for (XMLAttribute at : this.getAttributes()) {
				if (at.getName().equals(attr.getName())) {
					at.setValue(attr.getValue());
					return;
				}
			}
			this.getAttributes().add(attr);
		}

		/**
		 * Adds a attribute to the node
		 *
		 * @param key
		 * @param value
		 */
		public void addAttribute(String key, String value) {
			this.addAttributePrivate(new XMLAttribute(key, value));
		}

		/**
		 * Adds a attribute to the node
		 *
		 * @param key
		 * @param value
		 */
		public void addAttribute(String key, double value) {
			this.addAttributePrivate(new XMLAttribute(key, value));
		}

		/**
		 * Adds a attribute to the node
		 *
		 * @param key
		 * @param value
		 */
		public void addAttribute(String key, Float value) {
			this.addAttributePrivate(new XMLAttribute(key, value));
		}

		/**
		 * Adds a attribute to the node
		 *
		 * @param key
		 * @param value
		 */
		public void addAttribute(String key, Integer value) {
			this.addAttributePrivate(new XMLAttribute(key, value));
		}

		/**
		 * Adds a attribute to the node
		 *
		 * @param key
		 * @param value
		 */
		public void addAttribute(XMLAttribute attr) {
			this.addAttributePrivate(attr);
		}

		/**
		 * Adds a child to the node
		 *
		 * @param child
		 */
		public void addChild(XMLNode child) {
			this.getChilds().add(child);
			child.setParent(this);
		}

		/**
		 * Returns an attribute exists based in its name
		 */
		public XMLAttribute getAttribute(String name) {
			for (XMLAttribute a : this.getAttributes()) {
				if (a.getName().equals(name)) {
					return a;
				}
			}
			return null;
		}

		/**
		 * Returns a child
		 *
		 * @param index
		 * @return
		 */
		public XMLNode getChild(int index) {
			return this.getChilds().get(index);
		}

		/**
		 * Returns a child given by the name of the node
		 *
		 * @param index
		 * @return
		 */
		public XMLNode getChild(String nodeName) {
			for (XMLNode node : this.getChilds()) {
				if (node.getName().equals(nodeName)) {
					return node;
				}
			}
			return null;
		}

		/**
		 * Checks whether a child node exists or not
		 *
		 * @param nodeName
		 * @return
		 */
		public boolean nodeExists(String nodeName) {
			for (XMLNode node : this.getChilds()) {
				if (node.getName().equals(nodeName)) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Returns weather an attribute exists based in its name
		 */
		public boolean attributeExists(String name) {
			for (XMLAttribute a : this.getAttributes()) {
				if (a.getName().equals(name)) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Heloer function. Checks of <code>s</code> is contained in <code>list</code>
		 *
		 * @param list
		 * @param s
		 * @return
		 */
		private static boolean containsStringInList(List<String> list, String s) {
			for (String str : list) {
				if (str.equalsIgnoreCase(s)) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Checks if every attribute in the <code>attrs</code> array is an attribute of this node
		 *
		 * @param attrs
		 * @return
		 */
		public boolean attributesExists(String[] attrs) {
			List<String> attributeList = new ArrayList<String>(Arrays.asList(attrs));
			for (XMLAttribute a : this.getAttributes()) {
				if (!containsStringInList(attributeList, a.getName())) {
					return false;
				}
			}
			return true;
		}

		/**
		 * @return The childs array size
		 */
		public int getNumChilds() {
			return this.childs.size();
		}

		/**
		 * Set's the parent for this node
		 *
		 * @param parent
		 */
		public void setParent(XMLNode parent) {
			this.parent = parent;
		}

		/**
		 * Return the parent of this node
		 *
		 * @return
		 */
		public XMLNode getParent() {
			return this.parent;
		}

		/**
		 * Returns a formated XMLString
		 *
		 * @return
		 */
		public String getXMLString() {
			return this.getXMLString(0);
		}

		private String getXMLString(int height) {
			String s = "";
			for (int j = 0; j < height; j++) {
				s = s + "\t";
			}
			s = s + "<" + this.name + this.getAttributeString() + (this.getNumChilds() == 0 && this.getValue() == null ? " /" : "") + ">\n";
			if (this.childs.size() != 0) {
				for (int i = 0; i < this.childs.size(); i++) {
					s = s + this.childs.get(i).getXMLString(height + 1);
				}
				for (int j = 0; j < height; j++) {
					s = s + "\t";
				}
				s = s + "</" + this.name + ">\n";
			} else {
				if (this.getValue() != null) {
					s = s + this.getValue() + "\n";
					for (int j = 0; j < height; j++) {
						s = s + "\t";
					}
					s = s + "</" + this.name + ">\n";
				}
			}
			return s;
		}

		/**
		 * Creates an String with the attributes of this node
		 *
		 * @return
		 */
		private String getAttributeString() {
			String s = "";
			for (int i = 0; i < this.attributes.size(); i++) {
				s = s + this.attributes.get(i).getName() + "=\"" + this.attributes.get(i).getValue() + "\" ";
			}
			s = s.trim();
			if (this.attributes.size() > 0) {
				s = " " + s;
			}
			return s;
		}

		/**
		 * @return the childs
		 */
		public ArrayList<XMLNode> getChilds() {
			return this.childs;
		}

		/**
		 * @param childs
		 *            the childs to set
		 */
		public void setChilds(ArrayList<XMLNode> childs) {
			this.childs = childs;
		}

		/**
		 * @return the attributes
		 */
		public ArrayList<XMLAttribute> getAttributes() {
			return this.attributes;
		}

		/**
		 * @param attributes
		 *            the attributes to set
		 */
		public void setAttributes(ArrayList<XMLAttribute> attributes) {
			this.attributes = attributes;
		}

		/**
		 * @return the descriptive string for the xmlnode
		 */
		@Override
		public String toString() {
			return "[[Name|" + this.getName() + "][Childs|" + this.getChilds().size() + "][Attr.|" + this.getAttributes().size() + "]]";
		}

		/**
		 * Wrapper Method to print an XMLNode
		 *
		 * @param node
		 */
		public static void printNode(XMLNode node) {
			pNode(node, 0);
		}

		/**
		 * Prints an XMLNode with tabIndex as right shift amount
		 *
		 * @param node
		 * @param tabIndex
		 */
		private static void pNode(XMLNode node, int tabIndex) {
			String tabs = "";
			for (int i = 0; i < tabIndex; i++) {
				tabs += "  ";
			}
			String tabs2 = tabs + "  ";
			System.out.println(tabs + "[" + node.getName() + "] {");
			if (node.getValue() != null) {
				System.out.println(tabs2 + node.getValue());
			}
			for (XMLAttribute attr : node.getAttributes()) {
				System.out.println(tabs2 + attr.getName() + " - " + attr.getValue());
			}
			for (int i = 0; i < node.getChilds().size(); i++) {
				pNode(node.childs.get(i), tabIndex + 1);
			}
			System.out.println(tabs + "}");
		}

	}

	public static class XMLParser {

		/**
		 * @Author PikajuTheBoss
		 * @github https://github.com/Pikaju
		 */

		/**
		 * Parses a XMLString @param text
		 *
		 * @param text
		 * @return
		 * @throws EOFException
		 */
		public static XMLNode parseText(String text, XMLConfig cfg) throws EOFException {
			XMLNode currentNode = new XMLNode();
			char[] c = text.toCharArray();
			for (int i = 0; i < c.length; i++) {
				if (c[i] == '<' && c[i + 1] == '?') {
					i = text.indexOf("?>");
				}
				if (c[i] == '/') {
					if (currentNode.getParent().getName() == null) {
						return currentNode;
					}
					currentNode = currentNode.getParent();
				}
				if (c[i] == '<' && c[i + 1] != '/') {
					String tagText = text.substring(i + 1);
					XMLNode newNode = new XMLNode();
					currentNode.setValue(null);
					currentNode.addChild(newNode);
					currentNode = newNode;
					currentNode.setName(tagText.substring(0, tagText.indexOf(">")).split(" ")[0]);
					int valueStart = tagText.indexOf(">");
					boolean nextV = true;
					boolean found = false;
					for (int it = valueStart + 1; it < tagText.length(); it++) {
						if (!(tagText.charAt(it) == '\r' || tagText.charAt(it) == '\n' || tagText.charAt(it) == '\t' || tagText.charAt(it) == ' ')) {
							found = true;
							break;
						}
						if (tagText.charAt(it) == '<' && tagText.charAt(it + 1) == '/') {
							if (!found) {
								nextV = false;
							}
						}
					}
					if (found && nextV) {
						String v = new String(tagText.substring(tagText.indexOf(">", valueStart) + 1, tagText.indexOf("</", valueStart - 2))).trim();
						currentNode.setValue((v.trim().isEmpty() ? null : v));
					}
					int tagEnd = min(tagText.indexOf(">"), tagText.indexOf("/"));

					String attribText = tagText.substring(0, tagEnd);
					for (String currentAttribute : attribText.split(" ")) {
						if (currentAttribute.split("=").length >= 2) {
							currentNode.addAttribute(currentAttribute.split("=")[0], currentAttribute.split("=")[1].replace("\"", ""));
						}
					}
					i = i + tagEnd - 1;
				}
			}

			throw new EOFException("Nodes aren't closed properly");
		}

		/**
		 * Reads a file and passes it to parseText
		 *
		 * @param filePath
		 * @return
		 * @throws EOFException
		 */
		public static XMLNode parseFile(String filePath, XMLConfig cfg) throws EOFException {
			StringBuilder text = new StringBuilder();
			String line = "";
			try {
				BufferedReader reader = new BufferedReader(new FileReader(new File(filePath)));

				while ((line = reader.readLine()) != null) {
					text.append(line);
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return parseText(text.toString(), cfg);
		}

		/**
		 * Passes @param lines as single string to parseText
		 *
		 * @param lines
		 * @return
		 * @throws EOFException
		 */
		public static XMLNode parseFileLines(ArrayList<String> lines, XMLConfig cfg) throws EOFException {
			StringBuilder text = new StringBuilder();
			for (int i = 0; i < lines.size(); i++) {
				text.append(lines.get(i));
			}
			return parseText(text.toString(), cfg);
		}

		/**
		 * A minimal function for two integers
		 *
		 * @param a
		 * @param b
		 * @return
		 */
		private static int min(int a, int b) {
			return a < b ? a : b;
		}
	}

}
