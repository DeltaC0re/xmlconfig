package net.ccmob.xml;

import net.ccmob.xml.XMLConfig.XMLNode;

/**
 * This File is licensed under the MIT License (file -> license)
 * 
 * @author Marcel Benning
 * @email marcel@ccmob.net
 * @website https://ccmob.net
 * @version 1.0
 * 
 */

public class XMLUtil {

	/**
	 * This functions allows you to check if a node matches your needs in regards to their attributes
	 * and their names.
	 * 
	 * @param XMLNode n | The node you want to perform the check on
	 * @param String nodeName | The wanted node name of the supplied node (XMLNode n)
	 * @param String[] attributes | an array of attribute names of the supplied node (XMLNode n)
	 */
	public static boolean NodeMatches(XMLNode n, String nodeName, String[] attributes){
		if(n != null && nodeName != null){
			if(! n.getName().equalsIgnoreCase(nodeName))
				return false;
			if(attributes != null){
				if(attributes.length > 0){
					for(String attr : attributes){
						if(!n.attributeExists(attr))
							return false;
					}
				}
			}
			return true;
		}
		return false;
	}
	
	public static XMLNode getNodeByPath(String nodePath, XMLNode parentNode){
		if(nodePath.contains(".")){
			String childNodeName = nodePath.split("[.]")[0];
			if(parentNode.getChild(childNodeName) != null){
				if(nodePath.split("[.]").length > 1){
					return getNodeByPath(nodePath.substring(nodePath.indexOf('.')+1), parentNode.getChild(childNodeName));
				}else{
					return parentNode.getChild(childNodeName);
				}
			}
		}else{
			if(!nodePath.trim().isEmpty()){
				if(parentNode.getChild(nodePath) != null){
					return parentNode.getChild(nodePath);
				}
			}
		}
		return null;
	}
	
	public static void addDefaultToPath(XMLNode defaultNode, XMLNode rootNode, String path){
		if(getNodeByPath(path + "." + defaultNode.getName(), rootNode) == null){
			addNodeToPath(defaultNode, rootNode, path);
		}
	}
	
	public static void addNodeToPath(XMLNode lastNode, XMLNode rootNode, String path){
		String[] subNodes = path.split("[.]");
		String sNode;
		XMLNode currentNode = rootNode;
		for(int i = 0;i<subNodes.length;i++){
			sNode = subNodes[i];
			System.out.println("sNode: " + sNode);
			if(currentNode.getChild(sNode) != null){
				currentNode = currentNode.getChild(sNode);
				continue;
			}else{
				currentNode.addChild(new XMLNode(sNode));
				currentNode = currentNode.getChild(sNode);
				continue;
			}
		}
		currentNode.addChild(lastNode);
	}
	
}
